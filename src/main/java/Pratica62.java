import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.Time;
import utfpr.ct.dainf.if62c.pratica.JogadorComparator;
import java.util.List;
import java.util.Collections;
/**
 * @author Marcelo Guimarães da Costa <marcelo@unicode.com.br> 
 */
public class Pratica62 {
    public static void main(String[] args) {
        Time time = new Time();
        time.addJogador("Goleiro1", new Jogador(1, "Fulano"));
        time.addJogador("Lateral1", new Jogador(4, "Ciclano"));
        time.addJogador("Atacante1", new Jogador(10, "Beltrano"));
        time.addJogador("Goleiro2", new Jogador(1, "João"));
        time.addJogador("Lateral2", new Jogador(7, "José"));
        time.addJogador("Atacante2", new Jogador(15, "Mário"));
        
        //ORDER BY nome DESC, numero ASC
        List<Jogador> lista = time.ordena(new JogadorComparator(false,true,false));
        
        System.out.println(lista);
        
        System.out.println("\njogador procurado:\n" + 
                Collections.binarySearch(
                        lista, new Jogador(1, "Fulano"), new JogadorComparator(false, true,false)));
    }
}
